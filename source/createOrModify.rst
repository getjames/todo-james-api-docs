Create or Modify Tasks
======================

To create or modify a task, send a `POST` request to `https://tasks.getjames.app/apiv1/createOrModify`. Your request body should include the following:

- `apiKey`: Your :doc:`API key </createKey>`
- :doc:`Task attributes </taskJSON>` sent as body keys - see the :doc:`task structure example </taskJSON>` for better understanding.

The default values for the task attributes are:

.. code-block:: JSON

    {
        "id": undefined,
        "name": "Task created via the API",
        "waiting": false,
        "inbox": false,
        "archived": false,
        "description": "",
        "assignedTo": "",
        "duration": 30,
        "archivedAt": null,
        "done": false, 
        "doneAt": null,
        "createdAt": <current date>,
        "projectId": "no_project", 
        "deadline": null, 
        "starting": null
    }

Some restrictions:

- `createdAt` cannot be in the future and cannot be omitted. If `createdAt` is invalid, it will be silently replaced with the current date and time.
- A task without a `name` and `duration` cannot be planned. For tasks without a `name` and `duration`, `inbox` will be automatically set to `false`.
- `archivedAt` and `doneAt` will be automatically changed whenever you switch `archived` or `done` to `true`.

To create a new task with a random ID, leave the ID field `undefined`. To modify a task with a known ID, just pass it as a parameter and the existing task will be overwritten.

The API endpoint will return the newly created task as JSON like described in :doc:`/taskJSON`.
