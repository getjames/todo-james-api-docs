Task Structure (JSON)
=====================

Task data is structured like this example:

.. code-block:: JSON

    {
    "id": "77snEoHFbHEmcT3Rap2A",
    "description": "",
    "archivedAt": null,
    "name": "Task title",
    "assignedTo": "Ta9zH2oXIsAUserIDvMHqnnvu2",
    "duration": 30,
    "starting": null, 
    "deadline": "2020-08-12T00:00:00.000Z", 
    "projectId": "UkFaTMCWAZBKFzcbF9EC",
    "inbox": false,
    "done": false,
    "waiting": false,
    "archived": false,
    "doneAt": null,
    "createdAt": "2020-06-17T12:24:10.155Z"
    }
