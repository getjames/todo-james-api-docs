Webhook registration
====================

You can register webhooks that James will send requests to on changes. For this, you first need an :doc:`API key </createKey>` and a type. Currently, the following types exist:

- `plan`: James will notify the webhook when a task is planned. This is the case if and only if the `inbox` attribute of a task switches from `true` to `false`.
- `plannedModify`: James will notify the webhook when a planned task is modified. This is the case if the task already had the `inbox` attribute set to `false` before the change and any attribute is modified. This means that putting back a task into the inbox also triggers the webhook.

Register a New Webhook
^^^^^^^^^^^^^^^^^^^^^^
You can have up to 10 webhook URLs per type registered to receive notifications on change. If you try to register more webhooks per type than that, you will get the HTTP response `412`.

Register a webhook by sending a `POST` request to `https://tasks.getjames.app/apiv1/registerWebhook`. Your request body needs to include

- `apiKey`: Your :doc:`API key </createKey>`
- `type`: A valid type (see above)
- `url` The webhook URL that James will notify on changes

Your webhook will receive a `POST` request on changes with a JSON body structured :doc:`like a task </taskJSON>`. The JSON object will include an additional property `isUpdate` which will be truthy if the task already existed before the change.

James will send a traditional `POST` request to your endpoint, like this:

.. code-block:: JavaScript

    fetch(url, {
        method: "POST", 
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ ...data, isUpdate})
    })

Unregister an Existing Webhook
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Unregister a webhook by sending a `POST` request to `https://tasks.getjames.app/apiv1/registerWebhook` (same as for registering it in the first place). Your request body needs to include all parameters needed for the registration, in addition to a truthy `unsubscribe` value:

- `unsubscribe`: Set to `1` for unregistering
