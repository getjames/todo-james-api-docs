API Documentation for GetJames.app
==================================

This is the API documentation for the intelligent task manager `James <https://getjames.app>`_. You can register webhooks for planned tasks and modified planned tasks, or you can create / modify existing tasks with it.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   createKey
   testAuth
   taskJSON
   registerWebhook
   createOrModify


* :ref:`genindex`
* :ref:`search`
