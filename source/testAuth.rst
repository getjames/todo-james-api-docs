Test the Authentication
=======================

Test that everything is working with your API key so far by sending a `POST` request to :command:`https://tasks.getjames.app/apiv1/testAuth` with the `apiKey` in the request body:

.. code-block:: JavaScript

    fetch("https://tasks.getjames.app/apiv1/testAuth", {
        method: "POST", 
        headers: {
            "Content-Type": "application/json"
        }, 
        body: JSON.stringify({ apiKey: "1234secret...abcdef" })
    }).then(async resp => console.log(await resp.text()));

If everything worked, you should get a JSON-formatted user data object as a response. Otherwise the server will respond with error 403.

The `metadata` field will contain user metadata like your email and your name. An example response could look like this:

.. code-block:: JSON

    {
        "id":"Ta9zH2oXyourUser1DvMHqnnvu2",
        "lastLogin":"2020-08-23T18:58:42.898Z",
        "createdAt":"2020-06-17T11:07:27.934Z",
        "periodStart":"2020-06-17T11:07:27.934Z",
        "periodEnd":"2020-07-01T12:07:27.000Z",
        "numberOfItems":109,
        "rid":"",
        "knownUserIds":[],
        "trial":true,
        "apiKey":"1234secret...abcdef",
        "isSetUp":true,
        "numberOfActiveItems":30,
        "webhooks":{
            "plannedModify":[
                "https://hooks.zapier.com/hooks/standard/6337764/<someZapierStuff>/"
            ],
            "plan":[
                "https://hooks.zapier.com/hooks/standard/6337764/<someZapierStuff>/"
            ]
        },
        "metadata":{
            "uid":"Ta9zH2oXyourUser1DvMHqnnvu2",
            "email":"support@getjames.app",
            "emailVerified":true,
            "displayName":"James",
            "disabled":false,
            "metadata":{
                "lastSignInTime":"Wed, 29 Jun 2020 14:19:18 GMT",
                "creationTime":"Wed, 17 Jul 2020 11:07:23 GMT"
            },
            "providerData":[
                {
                    "uid":"support@getjames.app",
                    "displayName":"James",
                    "email":"support@getjames.app",
                    "providerId":"password"
                }
            ]
        }
    }

If you get no error, everything works fine.
